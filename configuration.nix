{ config, pkgs, ... }:
{
  imports =
    [
      ./configuration-fonts.nix
      ./configuration-keys.nix
      ./configuration-packages.nix
      ./configuration-xserver.nix
      ./hardware-configuration.nix
      ./home-manager.nix
    ];

  hardware.bluetooth.enable = true;

  services.upower.enable = true;
  services.tor.enable = true;
  services.tor.client.enable = true;
  services.openssh.enable = true;
  services.printing.enable = true;
  services.actkbd.enable = true;

  services.connman.enable = true;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "nixos";
  networking.firewall.enable = false;
  networking.wireless.enable = true;
  networking.networkmanager.enable = false;

  nixpkgs.config.allowUnfree = true;


  time.timeZone = "Europe/Moscow";


  sound.enable = true;
#  nixpkgs.config.pulseaudio = true;
#  hardware.pulseaudio.enable = true;
#  hardware.pulseaudio.support32Bit = true;

  systemd.services.upower.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.mb = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "audio" "networkmanager" ];
    hashedPassword = "$6$a/X4yGja68SzIy$dKCjv1Ku0ER9pDOwMql8Bnm058m0ffoYED41waKo3E50ggWbaBlADWPT3q1TTxgQvhmmo6taxfl9bAggVhFHK0";
  };


  console.font = "cyr-sun16";
  console.keyMap = "ruwin_cplk-UTF-8";

  i18n.defaultLocale = "en_US.UTF-8";


  system.stateVersion = "20.09";
  system.autoUpgrade.enable = true;

  nix.optimise.automatic = true;
  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 7d";
  nix.gc.dates = "03:15";

  # swapDevices = [
  #   { device = "/var/swapfile";
  #     size = 32768; # MiB
  #   }
  # ];

}
