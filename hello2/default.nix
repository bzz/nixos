with import <nixpkgs> {};

# let hello = callPackage ./hello.nix {};

# in

{ stdenv, fetchurl, perl }:

stdenv.mkDerivation {
  name = "hello-2.1.1";
  builder = ./builder.sh;
  src = fetchurl {
    url = ftp://ftp.nluug.nl/pub/gnu/hello/hello-2.1.1.tar.gz;
    sha256 = "1md7jsfd8pa45z73bz1kszpp01yw6x5ljkjk2hx7wl800any6465";
  };
#  inherit perl;
  buildInputs= [ perl ];
  # postInstall =
  #   ''
  #     source $stdenv/setup;
  #     genericBuild;
  #   '';

}
# hello

# with import <nixpkgs> {};

# { stdenv, writeShellScriptBin }:

# let
#   myScript = writeShellScriptBin "hello" "echo Hello World";
# in
# stdenv.mkDerivation rec {
#   name = "hello-1.0.0";

#   buildInputs = [ myScript ];
#   unpackPhase = true;

#   shellHook = ''
#     echo Hi
#   '';
# }
