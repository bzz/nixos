{ config, pkgs, ... }:
{
  # To detect pressed key code: actkbd -nsd /dev/input/event12

#  sound.mediaKeys.enable = false;
#  programs.light.enable = true;
  services.actkbd.bindings = [
    { keys = [ 224 ]; events = [ "key" "rep" ]; command = "${pkgs.light}/bin/light -U 4"; }
    { keys = [ 225 ]; events = [ "key" "rep" ]; command = "${pkgs.light}/bin/light -A 4"; }
    { keys = [ 113 ]; events = [ "key" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master toggle"; }
    { keys = [ 114 ]; events = [ "key" "rep" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master 5-"; }
    { keys = [ 115 ]; events = [ "key" "rep" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master 5+"; }

    # { keys = [ 46 125 ]; events = [ "key" ]; command = "echo copy"; }
    # { keys = [ 47 125 ]; events = [ "key" ]; command = "echo paste"; }
    
  ];

}